http://blog.portalpravaler.com.br/quais-as-diferencas-entre-os-cursos-de-sistemas-de-informacao-e-ciencias-da-computacao/

#Sistemas de Informação

O curso de Sistemas de Informação tem um foco, que é o planejamento e desenvolvimento de sistemas de informação e automação. Há uma semelhança com Ciências da Computação, mas aqui, a diferença está no fato de que o estudante de Sistemas de Informação seria preparado para atuar de forma mais direta tanto no desenvolvimento de softwares quanto em atividades que têm relação mais próxima, como suporte.

O aluno do curso de Sistemas de Informação terá um aprendizado mais voltado para a solução de problemas de negócios e as necessidades tecnológicas relacionadas, principalmente no ambiente corporativo. Logo, é um curso mais voltado a lidar com planejamento, administração, negócios, relações humanas e desenvolvimento de sistemas de informação e automação de processos. A duração média do curso é de quatro anos e meio, abordando, além de disciplinas técnicas, matérias como economia e administração, por exemplo.

#Ciências da Computação

O curso de Ciências da Computação aborda de maneira mais aprofundada os conceitos e teorias da computação. Há um foco numa sólida formação em temas como estrutura de dados, algoritmos, análise e desenvolvimento de sistemas, entre outras especificidades. O trabalho nesta área é voltado essencialmente ao software, sendo um curso com grande embasamento em fundamentos matemáticos e em cálculos.

O aluno do curso de Ciências da Computação se volta à resolução de problemas reais, com a aplicação de soluções que envolvam a computação. Isto de forma independente do ambiente, que pode ser comercial, industrial ou científico. Com aplicação em diversas áreas do conhecimento, o curso possibilita muitas oportunidades para o formado.

Alunos egressos do curso de Ciências da Computação costumam seguir carreiras ligadas ao desenvolvimento de softwares. É fato que outras áreas podem ser exploradas, tais como segurança da informação ou estrutura de redes, por exemplo. O curso tem duração de quatro a cinco anos e a conclusão costuma estar atrelada à criação de um programa, bem como um trabalho a respeito dele.

[Faculdade: Engenharia de Software, Ciência da Computação ou Sistemas de Informação? // Vlog #29](https://www.youtube.com/watch?v=APmtMvaYMLg)

