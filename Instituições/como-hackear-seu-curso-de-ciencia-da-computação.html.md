## USP

[Grade curricular da USP](https://www.ime.usp.br/dcc/grad/grade)


### Matematica 7

MAT0112 Vetores e Geometria
MAT0111 Cálculo Diferencial e Integral I
MAT0121 Cálculo Diferencial e Integral II
MAT0211 Cálculo Diferencial e Integral III
MAT0221 Cálculo Diferencial e Integral IV
MAT0122 Álgebra Linear I
MAT0213 Álgebra II

### Estatística 3

MAE0121 Introdução à Probabilidade e à Estatística I
MAE0212 Introdução à Probabilidade e à Estatística II
MAE0228 Noções de Probabilidade e Processos Estocásticos

### Computação 18

MAC0105 Fundamentos de Matemática para a Computação
MAC0101 Introdução à Ciência da Computação
MAC0110 Introdução à Computação
MAC0121 Algoritmos e Estruturas de Dados I
MAC0323 Algoritmos e Estruturas de Dados II
MAC0211 Laboratório de Programação I
MAC0242 Laboratório de Programação II
MAC0329 Álgebra Booleana e Aplicações
MAC0239 Introdução à Logica e Verificação de Programas
MAC0300 Métodos Numéricos da Álgebra Linear
MAC0315 Programação Linear
MAC0316 Conceitos Fundamentais de Linguagens de Programação
MAC0338 Análise de Algoritmos
MAC0426 Sistemas de Bancos de Dados
MAC0332 Engenharia de Software
MAC0412 Organização de Computadores
MAC0422 Sistemas Operacionais
MAC0438 Programação Concorrente

### Física 2

4310126 Física I
4310137 Física II

### Letras 1

FLC0474 Língua Portuguesa

### Trabalho 2

MAC0499 Trabalho de Formatura Supervisionado(2 semestres)
MAC0499 Trabalho de Formatura Supervisionado(continuação)

### Optativas 2

10 Optativas
2 Livres

### Total = 45 matérias

## UFSC

### Introdução [2 horas-aula]

* Apresentação da UFSC/CTC/INE

* Corpo docente

* Infraestrutura disponível

### Apresentação do curso [4 horas-aula]

* Cursos de graduação em Computação e Informática no Brasil.

* Caracterização do curso de Ciências da Computação da UFSC.

* Projeto pedagógico do curso de Ciências da Computação da UFSC.

### Representação de informação [10 horas-aula]

* Tipos de dados

* Sistemas de numeração

* Bases

* Principais sistemas de numeração

* Operações em diferentes sistemas de numeração

* Conversões entre diferentes sistemas de numeração

### Computação digital [10 horas-aula]

* Princípios

* Representação de instruções

* Uma máquina hipotética

* Evoluções

### Componentes básicos de um computador [4 horas-aula]

* Conceitos fundamentais

* Hardware

* Linguagens de programação

* Sistemas operacionais

* Tecnologias

### Introdução às diversas áreas relacionadas à Ciência da Computação [6 horas-aula]

* Arquitetura de computador

* Teoria da computação

* Linguagens de computador

* Sistemas operacionais

* Redes de computadores

* Sistemas distribuídos

* Engenharia de software

* Matemática na computação

* Tecnologias da informação: bancos de dados, computação gráfica, segurança, inteligência artificial

* Computação e ética

* Software livre

## UFMG

Divisão das Materias

Ciencia da Computação = 54%
Matemática = 19%
Física = 13%
Ciencias Administrativas = 3%
Ciencias Contábeis = 3%
Ciencias Economicas = 3%
Estatística = 3%
Letras = 2%

[Bacharelado em Ciência da Computação | DCC - UFMG](http://www.dcc.ufmg.br/dcc/?q=pt-br/bcc)


## MIT

[Computer Science and Engineering (Course 6-3) < MIT](http://catalog.mit.edu/degree-charts/computer-science-engineering-course-6-3/)


## Scott Young

<center><iframe class="iframevideo" src="https://www.youtube.com/embed/piSLobJfZ3c" frameborder="0" allowfullscreen></iframe></center>

![](https://cdn-images-1.medium.com/max/2000/1*PeowzcqbiGo20ReN_9K-GQ.png)

![](https://cdn-images-1.medium.com/max/2000/1*o0NolpkEBauW1og_DAOvRA.jpeg)

[MIT Challenge](https://www.scotthyoung.com/blog/myprojects/mit-challenge-2/)


[Books & Courses](https://www.scotthyoung.com/blog/books-courses/)


[How to Start Your Own Ultralearning Project (Part One)](https://www.scotthyoung.com/blog/2016/07/28/ultralearn-diy-1/)


## Referencias
[Informações sobre o Bacharelado em Ciência da Computação | DCC](http://www.dcc.ufmg.br/dcc/?q=pt-br/informacoes_BCC)


[Departamento de Informática e Estatística](https://planos.inf.ufsc.br/modulos/planos/visualizar.php?disciplina=INE5401)


[Ementas das Disciplinas do Bacharelado em Ciência da Computação | DCC](http://www.dcc.ufmg.br/dcc/?q=pt-br/ementa_BCC)

